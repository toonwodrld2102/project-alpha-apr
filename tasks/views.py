from django.shortcuts import render, redirect, get_object_or_404
from tasks.forms import TaskForm
from tasks.models import Task
from django.contrib.auth.decorators import login_required


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm

    context = {
        "task_form": form,
    }
    return render(request, "tasks/create_task.html", context)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(creator=request.user)
    context = {
        "show_my_tasks": tasks,
    }
    return render(request, "tasks/show_my_tasks.html", context)

@login_required
def edit_task(request, id):
    tasks = get_object_or_404(Task, id=id)
    if request.method == "POST":
        form = TaskForm(request.POST, instance=tasks)
        if form.is_valid():
            form.save()
            return redirect("show_my_tasks")
    else:
        form = TaskForm(instance=tasks)
    context = {
        "task_object": tasks,
        "edit_form": form
    }
    return render(request, "tasks/task_edit.html", context)


# Create your views here.
