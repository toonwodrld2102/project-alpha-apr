from django.db import models
from django.contrib.auth.models import User
from projects.models import Project
from company.models import Employee


class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateField(null=True, blank=True)
    due_date = models.DateField(null=True, blank=True)
    is_completed = models.BooleanField(default=False)
    notes = models.TextField(null=True, blank=True)
    project = models.ForeignKey(
        Project, related_name="tasks", on_delete=models.CASCADE
    )
    creator = models.ForeignKey(
        User, null=True, related_name="tasks", on_delete=models.CASCADE
    )
    employee = models.ForeignKey(
        Employee,
        null=True,
        related_name="employee",
        on_delete=models.CASCADE
    )


# Create your models here.
