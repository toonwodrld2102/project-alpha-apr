from django.forms import ModelForm, DateInput
from tasks.models import Task

class DateInput(DateInput):
    input_type = "date"


class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "project",
            "is_completed",
            "notes",
            "creator",
            "employee",
        ]
        widgets = {
            "start_date": DateInput,
            "due_date": DateInput
        }
