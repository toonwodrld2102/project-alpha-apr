from django.forms import ModelForm
from company.models import Company, Employee


class CompanyForm(ModelForm):
    class Meta:
        model = Company
        fields = [
            "name",
            "description",
            "employee",
            "project",
        ]

class EmployeeForm(ModelForm):
    class Meta:
        model = Employee
        fields = [
            "name",
            "employee_id",
            "position",
            "department",
            "phone_number",
        ]
