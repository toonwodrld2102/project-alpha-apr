from django.db import models
from projects.models import Project
from phonenumber_field.modelfields import PhoneNumberField

class Employee(models.Model):
    name = models.CharField(max_length=200)
    employee_id = models.PositiveIntegerField()
    position = models.CharField(max_length=200)
    department = models.CharField(max_length=200)
    phone_number = PhoneNumberField(null=False, blank=False, unique=True)

    def __str__(self):
        return self.name



class Company(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    employee = models.ForeignKey(
        Employee,
        null=True,
        related_name="all_employee",
        on_delete=models.CASCADE
        )
    project = models.ManyToManyField(
        Project,
        blank=True,
        related_name="projects"
        )

    def __str__(self):
        return self.name


# class Employee(models.Model):
#     name = models.CharField(max_length=200)
#     employee_id = models.PositiveIntegerField()
#     position = models.CharField(max_length=200)
#     department = models.Charfield(max_length=200)
#     phone_number = models.PhoneNumberField(null=False, blank=False, unique=True)
