from django.shortcuts import render, redirect, get_object_or_404
from company.models import Company, Employee
from projects.models import Project
from django.contrib.auth.decorators import login_required
from company.forms import CompanyForm, EmployeeForm


@login_required
def create_company(request):
    if request.method == "POST":
        form = CompanyForm(request.POST)
        if form.is_valid():
            company = form.save(False)
            company.owner = request.user
            company.save()
            return redirect("list_projects")
    else:
        form = CompanyForm

    context = {
        "company_form": form,
    }
    return render(request, "company/create_company.html", context)


@login_required
def create_employee(request):
    if request.method == "POST":
        form = EmployeeForm(request.POST)
        if form.is_valid():
            employee = form.save(False)
            employee.save()
            return redirect("list_projects")
    else:
        form = EmployeeForm

    context = {
        "employee_form": form,
    }
    return render(request, "company/create_employee.html", context)


@login_required
def show_companies(request):
    companies = Company.objects.all()
    context = {
        "show_companies": companies,
    }
    return render(request, "company/show_companies.html", context)

@login_required
def company_detail(request, id):
    company_detail = get_object_or_404(Company, id=id)
    project = Project.objects.filter(projects = company_detail)
    employee = Employee.objects.filter(all_employee = company_detail)
    print(project)
    print(employee)
    context = {
        "company_detail": company_detail,
        "project": project,
        "all_employee": employee
    }
    return render(request, "company/company_detail.html", context)

@login_required
def employee_profile(request, id):
    employee_profile = get_object_or_404(Employee, id=id)
    context = {
        "employee_profile": employee_profile
    }
    return render(request, "company/employee_profile.html", context)
