from django.urls import path
from company.views import create_company, create_employee, show_companies, company_detail, employee_profile

urlpatterns = [
    path("employee/<int:id>/", employee_profile, name="employee_profile"),
    path("<int:id>/", company_detail, name="company_detail"),
    path("create/employee", create_company, name="create_company"),
    path("create/company", create_employee, name="create_employee"),
    path("", show_companies, name="show_companies"),
]
