from django.contrib import admin
from company.models import Company, Employee


# @admin.register(Company)
# class CompanyAdmin(admin.ModelAdmin):
#     list_display = ["name", "description", "employee", "project"]
admin.site.register(Company)


@admin.register(Employee)
class CompanyAdmin(admin.ModelAdmin):
    list_display = ["name", "employee_id", "position", "department", "phone_number"]


# Register your models here.
