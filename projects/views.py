from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


@login_required
def list_projects(request):
    lists = Project.objects.filter(owner=request.user)
    context = {
        "list_projects": lists,
    }
    return render(request, "projects/list_projects.html", context)


@login_required
def show_project(request, id):
    show_project = get_object_or_404(Project, id=id)
    context = {"show_project": show_project}
    return render(request, "projects/show_project.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm

    context = {
        "form": form,
    }
    return render(request, "projects/create_project.html", context)

@login_required
def edit_project(request, id):
    list = Project.objects.get(id=id)
    if request.method == "POST":
        form = ProjectForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
            return redirect("show_project", id=id)
    else:
        form = ProjectForm(instance=list)
    context = {
        "project_object": list,
        "edit_form": form
    }
    return render(request, "projects/edit_project.html", context)


# Create your views here.
